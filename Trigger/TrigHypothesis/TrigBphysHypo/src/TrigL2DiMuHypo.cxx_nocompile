/***************************************************************************
    Authors: Natalia Panikashvili
             Shlomit Tarem
***************************************************************************/

#include "TrigL2DiMuHypo.h"
#include "TrigT1Interfaces/TrigT1Interfaces_ClassDEF.h"
#include "GaudiKernel/IDataProviderSvc.h"
#include "GaudiKernel/IIncidentSvc.h"
#include "GaudiKernel/PropertyMgr.h"
#include "GaudiKernel/SmartDataPtr.h"
//#include "TrigInDetEvent/TrigInDetTrack.h"
//#include "TrigParticle/TrigL2Bphys.h"
//#include "TrigParticle/TrigL2BphysContainer.h"
#include "TrigSteeringEvent/TrigPassBits.h"
#include "TrigNavigation/Navigation.h"
#include "xAODTrigBphys/TrigBphysContainer.h"
#include "xAODTrigBphys/TrigBphys.h"
#include "xAODTracking/TrackParticle.h"
#include "xAODTracking/TrackParticleContainer.h"

using namespace std;

TrigL2DiMuHypo::TrigL2DiMuHypo(const std::string& name, ISvcLocator* pSvcLocator) :
    HLT::HypoAlgo(name, pSvcLocator),
    m_roiNum          (0),
    m_jpsiNum         (0),
    m_pStoreGate      (NULL)
{
  declareProperty("MuMuMassMin", m_MassMin = 2800.);
  declareProperty("MuMuMassMax", m_MassMax = 4000.);
  declareProperty("ApplyOppositeCharge", m_ApplyOppCharge = true);
  declareProperty("ApplyMuMuMassMax", m_ApplyMassMax = true);
  declareProperty("AcceptAll",   m_acceptAll = false);
  declareProperty("NHits",  m_NHits  =  3);
  declareProperty("ApplyChi2Cut",  m_ApplyChi2Cut  =  false);
  declareProperty("Chi2VtxCut",  m_Chi2VtxCut  =  20);

  // monitoring variables
  declareMonitoredVariable(    "NBphys",   m_mon_nbphys);
  declareMonitoredStdContainer("MuMumass", m_mon_dimumass, AutoClear);
  declareMonitoredStdContainer("MuEta"   , m_mon_muEta   , AutoClear);
  declareMonitoredStdContainer("MuPhi"   , m_mon_muPhi   , AutoClear);
}

TrigL2DiMuHypo::~TrigL2DiMuHypo()
{}

HLT::ErrorCode TrigL2DiMuHypo::hltInitialize()
{
  msg() << MSG::INFO << "Initializing TrigL2DiMuHypo" << endmsg;
  return HLT::OK;
}

HLT::ErrorCode TrigL2DiMuHypo::hltFinalize()
{
  msg() << MSG::INFO << "Finalizing TrigL2DiMuHypo" << endmsg;
  return HLT::OK;
}

HLT::ErrorCode TrigL2DiMuHypo::hltExecute(const HLT::TriggerElement* outputTE, bool& pass)
{
  pass=false;
  m_mon_nbphys=0;
  if (msgLvl() <= MSG::DEBUG)
    msg() << MSG::DEBUG << " Executing TrigL2DiMuHypo " << endmsg;

  m_pStoreGate = store();

  if (m_acceptAll)
  {
    pass = true;
    if (msgLvl() <= MSG::DEBUG)
      msg() << MSG::DEBUG << "Accept property is set: taking all the events" << endmsg;
    return HLT::OK;
  }

  ++m_roiNum;
  int pair_number = 0;
  bool result = false;

    // get vector for TrigL2Bphys particles
    const xAOD::TrigBphysContainer* trigBphysColl(nullptr);
  HLT::ErrorCode status = getFeature(outputTE, trigBphysColl);
  if (status != HLT::OK )
  {
    if (msgLvl() <= MSG::WARNING)
      msg() << MSG::WARNING << "Failed to get TrigBphysics collection" << endmsg;
    return HLT::OK;
  }

  if (trigBphysColl == NULL )
  {
    if (msgLvl() <= MSG::DEBUG )
      msg() << MSG::DEBUG << "No Bphys particles found" << endmsg;
    return HLT::OK;
  }

  if (msgLvl() <= MSG::DEBUG )
    msg() << MSG::DEBUG << "Got TrigBphys collection with " << trigBphysColl->size() << endmsg;

  TrigPassBits *bits = HLT::makeTrigPassBits(trigBphysColl);

  m_mon_nbphys=trigBphysColl->size();

  xAOD::TrigBphysContainer::const_iterator
      thePair = trigBphysColl->begin(),
  endPair = trigBphysColl->end();
  for (; thePair != endPair;  thePair++)
  {
    if ((*thePair)->particleType() == xAOD::TrigBphys::JPSIMUMU)
    {
      float mass = (*thePair)->mass();
      if (m_MassMin < mass && (mass < m_MassMax || (!m_ApplyMassMax)) )
      {
        // apply chi2 cut if requested
        if( m_ApplyChi2Cut && (*thePair)->fitchi2() > m_Chi2VtxCut) continue;
        
        pair_number++;
        if (msgLvl() <= MSG::DEBUG)
          msg() << MSG::DEBUG << " Pair_number " << pair_number << " Invariant mass after cut = " << mass << endmsg;
        // const ElementLinkVector<TrigInDetTrackCollection> trackVector = (*thePair)->trackVector();
          typedef std::vector<ElementLink<xAOD::TrackParticleContainer > > VecEL;
          const VecEL trackVector = (*thePair)->trackParticleLinks();
          
        if (msgLvl() <= MSG::DEBUG)
          msg() << MSG::DEBUG << "Number of tracks in vertex:  " << trackVector.size() << endmsg;

        //check that number of tracks is 2
        if(trackVector.size()!=2) {
          msg() << MSG::WARNING << "Number of trigger tracks in J/psi vertex differs from 2 " << endmsg;
          continue;
        }

        // check broken ElementLinks
        if( !trackVector.at(0).isValid() || !trackVector.at(1).isValid() ) {
          msg() << MSG::WARNING << "Broken ElementLink." << endmsg;
          return HLT::ErrorCode(HLT::Action::CONTINUE, HLT::Reason::USERDEF_1);
        }

        //VecEL::const_iterator  it=trackVector.begin(),  itEnd=trackVector.end();
        // int goodTracks = 0;
        std::vector<float> pT, eta, phi;
          /* JW Not yet adapted
        for (int itrk=0; it != itEnd; ++it, ++itrk)
        {
          const TrigInDetTrackFitPar* p_param=(*(*it))->param();
          if (p_param == NULL )
          {
            if (msgLvl() <= MSG::WARNING )
              msg() << MSG::WARNING << "No track parameters for track  " << itrk << endmsg;
            continue;
          }
          if (msgLvl() <= MSG::DEBUG)
            msg() << MSG::DEBUG << "Parameters for track " << itrk
                << " pT = " << p_param->pT()
                << " phi = " << p_param->phi0()
                << " eta = " << p_param->eta() << endmsg;

          pT .push_back(p_param->pT());
          eta.push_back(p_param->eta());
          phi.push_back(p_param->phi0());
          if (msgLvl() <= MSG::DEBUG)
            msg() << MSG::DEBUG << " HitPattern() : " << (*(*it))->HitPattern() << endmsg;
          int32_t word = (*(*it))->HitPattern();
          Encoding* pEncoding = (Encoding*)&word;

          if (msgLvl() <= MSG::DEBUG)
            msg() << MSG::DEBUG << " mdt : " << pEncoding->mdt
                << " tgc : " << pEncoding->tgc
                << " rpc : " << pEncoding->rpc << endmsg;

          int32_t mdt_hits = pEncoding->mdt;
          int32_t tgc_hits = pEncoding->tgc;
          int32_t rpc_hits = pEncoding->rpc;

          if ((mdt_hits + rpc_hits) >= m_NHits || (mdt_hits + tgc_hits) >= m_NHits)
          {
            goodTracks++;
            if (msgLvl() <= MSG::DEBUG)
              msg() << MSG::DEBUG << " goodTracks : " << goodTracks << endmsg;
          }
        }
           */
          if (msgLvl() <= MSG::WARNING ) {
              msg() << MSG::WARNING << "Code in TrigL2DiMuHypo not fully implemented:  " << endmsg;
              continue;
          }
        /* ST: commented out to prevent Coverity warning
        if (goodTracks == 2)
        {
          if (msgLvl() <= MSG::DEBUG)
            msg() << MSG::DEBUG << " goodTracks == 2, pTs: " << pT[0] << " " << pT[1] << " save mass  " << goodTracks << endmsg;

          if (m_ApplyOppCharge) {
            if ((pT[0] * pT[1]) < 0.) {
              result = true;
              HLT::markPassing(bits, *thePair, trigBphysColl);
              m_mon_dimumass.push_back( mass/CLHEP::GeV );
              m_mon_muEta.push_back(eta[0]);
              m_mon_muEta.push_back(eta[1]);
              m_mon_muPhi.push_back(phi[0]);
              m_mon_muPhi.push_back(phi[1]);
            }
          } else {
            result=true;
            HLT::markPassing(bits, *thePair, trigBphysColl);
            m_mon_dimumass.push_back( mass/CLHEP::GeV );
            m_mon_muEta.push_back(eta[0]);
            m_mon_muEta.push_back(eta[1]);
            m_mon_muPhi.push_back(phi[0]);
            m_mon_muPhi.push_back(phi[1]);
          }
        }
        */
      }
    }
  }

  // store result
  if ( attachBits(outputTE, bits) != HLT::OK ) {
    msg() << MSG::ERROR << "Problem attaching TrigPassBits! " << endmsg;
  }

  pass = result;
  return HLT::OK;

}





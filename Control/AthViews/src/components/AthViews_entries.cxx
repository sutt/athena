#include "../ViewDataVerifier.h"
#include "../ViewTestAlg.h"
#include "../RoiCollectionToViews.h"

DECLARE_COMPONENT( AthViews::ViewDataVerifier )
DECLARE_COMPONENT( AthViews::ViewTestAlg )
DECLARE_COMPONENT( AthViews::RoiCollectionToViews )

